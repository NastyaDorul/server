const { Router } = require("express");

const router = Router();

// Require controller modules.
var womans_controller = require("../controllers/womansController");

//-По-Гет-запросу----отображение-данных-для-сортировки-womans
router.get("/sorting", womans_controller.sorting);

//-По-Гет-запросу----отображение-данных-из-Femaleproducts
router.get("/femaleproducts", womans_controller.femaleproducts);

//-По-Гет-запросу----отображение-данных-из-Femaleproducts
router.get(
  "/femaleproducts/:filterKey",
  womans_controller.femaleproducts_filter
);

//-По-Гет-запросу----пагинация-в-категории--NestedItemGameConsoles
router.get("/allproducts", womans_controller.allproducts);

//-По-Гет-запросу----отображение-данных-из-коллекции-Femaleproducts--по _id
router.get("/search-By/:id", womans_controller.searchBy_id);

//-По-Гет-запросу----отображение данных из коллекции NestedItemGames по значению поля "title"
router.get("/search/products/:title", womans_controller.searchProducts_title);

module.exports = router;
