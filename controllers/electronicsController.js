const {
  NestedItemGames,
  Gaming,
  NestedItemGameConsoles,
  ElectronicFilters,
} = require("../models/Gaming");

//-----------------------По-Гет-запросу----добавляю данные в БД-------------------------------------------------------

exports.gaming_create = function (req, res) {
  const gaming = new Gaming({
    name: "Gaming",
  });

  gaming.save(function (err) {
    if (err) return console.log(err);

    const mySpanishCoach = new NestedItemGames({
      title: "My Spanish Coach(for Sony PSP)",
      price: 49.99,
      availability: "In Stock",
      description: "Learn Spanish in a fun and interactive way",
      brand: "No",
      gamingSystem: "Sony PSP",
      genre: "Educational",
      gameRating: "E",
      parentCategory: gaming._id,
      image: "https://images-na.ssl-images-amazon.com/images/I/51aVVW1-TtL.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    mySpanishCoach.save(function (err) {
      if (err) return console.log(err);
    });

    const SegaBassFishing = new NestedItemGames({
      title: "Sega Bass Fishing(for Wii)",
      price: 39.99,
      availability: "In Stock",
      description:
        "The sun is out, the fish are biting and your tackle box is filled with 20 different types of lures. With content created exclusively for the Wii, you'll motor out, find your favorite fishing hole, cast off using the Wii Remote? and snag one of four types of bass. When you hook a fighter, your motion sensor will let you know, so hang on and enjoy the ride!",
      brand: "No",
      gamingSystem: "Wii",
      genre: "Kids,Sports",
      gameRating: "E",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dw1c2b6867/images/large/sega-bass-fishing-wii.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    SegaBassFishing.save(function (err) {
      if (err) return console.log(err);
    });

    const MidwayCruisn = new NestedItemGames({
      title: "Midway Cruis'n(for Wii)",
      price: 49.99,
      availability: "In Stock",
      description:
        "An all new version of the classic Midway arcade racer, Cruis'n is coming exclusively to the Wii. Choose your ride from an impressive lineup of licensed cars and experience a rush of adrenaline as you race opponents through twelve different street circuits using the Wii Remote™ to steer and perform outrageous stunts. Win races and earn upgrades including: turbos, body kits, neon and nitrous, allowing you to create the ultimate ride.",
      brand: "Midway",
      gamingSystem: "Wii",
      genre: "Racing",
      gameRating: "E",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dwb06cd931/images/large/midway-cruisn-wii.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    MidwayCruisn.save(function (err) {
      if (err) return console.log(err);
    });

    const PipeMania = new NestedItemGames({
      title: "Pipe Mania(for Sony PSP)",
      price: 49.99,
      availability: "In Stock",
      description:
        "Pipemania is a fast-paced puzzle game in which players lay down a pre-ordained set of pipes on a tiled grid in order to keep the Flooze, a constant flowing substance that speeds up with progression, moving through the pipes as long as possible without overflowing. Players will need to be fast with their fingers and utilize quick and forward thinking, precise coordination, and keen awareness if they plan to survive the time constraints, pressure and accuracy needed to navigate through the game. There is no chance for players to let their guard down, as complication increases with new elements as players progress through the game.",
      brand: "Atari",
      gamingSystem: "Sony PSP",
      genre: "Strategy",
      gameRating: "E",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dwc06eee61/images/large/atari-pipe-mania-psp.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    PipeMania.save(function (err) {
      if (err) return console.log(err);
    });

    const LegoIndianJones = new NestedItemGames({
      title: "Lego Indian Jones: The Original Adventure(for Wii)",
      price: 49.99,
      availability: "In Stock",
      description:
        "Once again rendering a beloved George Lucas creation in LEGOs, LEGO Indiana Jones features the intrepid explorer in an epic adventure based on the film series. Developed by the same team that created the LEGO Star Wars series, LEGO Indiana Jones presents a tongue-in-cheek take on the first three cinematic adventures of pop culture's most iconic archaeologist, including Indiana Jones and the Raiders of the Lost Ark, Indiana Jones and the Temple of Doom and Indiana Jones and the Last Crusade.",
      brand: "LucasArts",
      gamingSystem: "Wii",
      genre: "Kids",
      gameRating: "E",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dwb1927240/images/large/lucasarts-lego-indiana-jones-original-adventure-wii.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    LegoIndianJones.save(function (err) {
      if (err) return console.log(err);
    });

    const WeSki = new NestedItemGames({
      title: "We Ski(for Wii).Sports",
      price: 39.99,
      availability: "In Stock",
      description:
        "Welcome to the Happy Ski Resort, where fresh powder, groomed runs, multiple trails, and state of the art facilities await! We Ski™ takes you down the slopes in the most exciting skiing game to hit the Wii™! Grab your Wii Remote™ and Nunchuk™ or step onto your Wii Balance Board™ to execute perfect wedge stops, shred the slaloms, and negotiate moguls with ease. With an in-depth Ski School and over a dozen lengthy runs packed with jumps, races, and more, you and your friends won’t want to leave the slopes again!",
      brand: "Namco",
      gamingSystem: "Wii",
      genre: "Sports",
      gameRating: "E",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dwf76c95ec/images/large/namco-we-ski-wii.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    WeSki.save(function (err) {
      if (err) return console.log(err);
    });

    const BigBrainAcademy = new NestedItemGames({
      title: "Big Brain Academy(for Nintendo DS)",
      price: 29.99,
      availability: "In Stock",
      description:
        "A stress reliever application for the DS, Big Brain Academy features 15 activities that test your brain powers in areas like logic, memory, math and analysis. Free your mind by working through a number of simple yet deep problems. There are five different types of challenges. The game is playable by all ages, with only one cartridge needed for eight players and each activity takes less than a minute to complete.",
      brand: "Nintendo",
      gamingSystem: "Nintendo DS",
      genre: "Educational",
      gameRating: "E",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dw7301a667/images/large/nintendo-big-brain-academy-nintendods.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    BigBrainAcademy.save(function (err) {
      if (err) return console.log(err);
    });

    const MaddenNFL09 = new NestedItemGames({
      title: "Madden NFL09(for Wii)",
      price: 49.99,
      availability: "In Stock",
      description:
        "Now enhance the core skills areas of football and experience to match your playing style with the Madden NFL 09 Wii game from Electronic Arts™. An all-new Wii exclusive playcalling system featuring simplified one-button controls and new assisted player movement make Madden NFL 09 the most accessible Wii football game ever. This game is incorporated with All-New 5-on-5 Mode Neighborhood-style tackle football jumps to life exclusively on the Wii. The fast-paced game play and a smaller playing field deliver high-scoring games that are over-the-top and in-your-face. In these game players of all skill levels can be competitive by calling plays from three unique playbooks, ranging from Easy to Advanced. Intelligent Coach Tips provide instant feedback to help you become a better player on the fly. Control receivers and make spectacular catches or key possession receptions, or roam the secondary and jar the ball loose with big hits and ball-stripping tackles, as you indulge yourself in heart-thumping sporting action.",
      brand: "EASports",
      gamingSystem: "Wii",
      genre: "Sports",
      gameRating: "E",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dw0da5ec80/images/large/easports-madden-nfl-09-wii.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    MaddenNFL09.save(function (err) {
      if (err) return console.log(err);
    });

    const Nascar09 = new NestedItemGames({
      title: "Nascar 09(for Sony PS3)",
      price: 59.99,
      availability: "In Stock",
      description:
        "Race your way with unprecedented adaptability and customization in NASCAR 09. As the sport's newest prodigy, NASCAR's four-time champion Jeff Gordon takes you under his wing and guides you through some of the key aspects of the game. Use his advice to your advantage when racing in an all-new Career mode or the Sprint Driver Challenges to earn reputation and become a NASCAR champion yourself. Create a racing credential, design a customized car, and get ready to race for premier contracts as you work your way to the top with NASCAR 09.",
      brand: "EASports",
      gamingSystem: "Sony PS3",
      genre: "Racing",
      gameRating: "E",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dw30b1cb2e/images/large/easports-nascar-09-ps3.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    Nascar09.save(function (err) {
      if (err) return console.log(err);
    });

    const MarioAndSonic = new NestedItemGames({
      title: "Mario and Sonic @The 2008 Bejing Olympic Games(for Wii)",
      price: 49.99,
      availability: "In Stock",
      description:
        "Featuring an all-star cast of characters from the amazing worlds of both Mario and Sonic, players will be able to compete as or against a range of familiar characters including Mario, Sonic, Luigi, Knuckles, Yoshi, Tails and more. Innovative usage of the Wii and DS control systems to maneuver your favorite character will allow players to race the likes of Mario and Sonic down the 100m track, leap over the high jump or churn water in a swimming heat, all while competing for the much sought-after Olympic gold medal. With up to four players on the Wii and on the DS, the stage is set to catch the fever this holiday with Mario and Sonic.",
      brand: "Sega",
      gamingSystem: "Wii",
      genre: "Kids",
      gameRating: "E",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dw0731ef7f/images/large/sega-mario-sonic-olympics-2008-wii.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    MarioAndSonic.save(function (err) {
      if (err) return console.log(err);
    });

    const QuickYoga = new NestedItemGames({
      title: "Quick Yoga Training(for Nintendo DS)",
      price: 29.99,
      availability: "In Stock",
      description:
        "Personalize your yoga routine with Quick Yoga Training. This game has over 180 poses, arranged and motion-captured by a certified instructor. It adapts to your level, and even evaluates your breathing technique. Practice hands-free with Voice Navigation and Voice Recognition.",
      brand: "Ubi Soft",
      gamingSystem: "Nintendo DS",
      genre: "Educational",
      gameRating: "E",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dw2754e9f7/images/large/ubi-soft-quick-yoga-training-nintendods.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    QuickYoga.save(function (err) {
      if (err) return console.log(err);
    });

    const StarWars = new NestedItemGames({
      title: "Star Wars: The Force Unleased(for Sony PSP)",
      price: 49.99,
      availability: "In Stock",
      description:
        "The Star Wars saga continues in Star Wars: The Force Unleashed. You are Darth Vader's Secret Apprentice! The Dark Lord of the Sith himself has trained you to use a lightsaber and unleash the power of the Force, but now your destiny is your own. Will you join Vader as the next great Sith, or will you choose to defend peace and justice as a noble Jedi Knight? The expansive story, created under direction from George Lucas, is set during the largely unexplored era between Star Wars: Episode III Revenge of the Sith and Star Wars: Episode IV A New Hope.",
      brand: "LucasArts",
      gamingSystem: "Sony PSP",
      genre: "Action",
      gameRating: "T",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dwf8f3825d/images/large/lucasarts-star-wars-the-force-unleashed-psp.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    StarWars.save(function (err) {
      if (err) return console.log(err);
    });

    const GrandTheftAuto = new NestedItemGames({
      title: "Grand Theft Auto 4(for X-Box 360)",
      price: 59.99,
      availability: "In Stock",
      description:
        "What does the American Dream mean today? For Niko Bellic, fresh off the boat from Europe, it is the hope he can escape his past. For his cousin, Roman, it is the vision that together they can find fortune in Liberty City, gateway to the land of opportunity. As they slip into debt and are dragged into a criminal underworld by a series of shysters, thieves and sociopaths, they discover that the reality is very different from the dream in a city that worships money and status, and is heaven for those who have them and a living nightmare for those who don’t.",
      brand: "Rockstar Games",
      gamingSystem: "X-Box 360",
      genre: "Racing",
      gameRating: "M",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dwda0fbe86/images/large/rockstar-games-grand-theft-auto-iv-xbox360.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    GrandTheftAuto.save(function (err) {
      if (err) return console.log(err);
    });

    const FightNight = new NestedItemGames({
      title: "Fight Night: Round 3(for X-Box 360)",
      price: 59.99,
      availability: "In Stock",
      description:
        "The highly acclaimed EA SPORTS Fight Night franchise, the undisputed champion of boxing videogames, delivers another hit with EA SPORTS Fight Night Round 3. Feel what it's like to hit—or be hit by—great champions such as Muhammad Ali and Oscar De La Hoya with film-quality graphics that showcase the most devastating punches seen in any game or movie. Outside the ring, establish intense rivalries by calling out opponents and triggering press conference brawls on your way to becoming boxing's greatest legend. EA SPORTS Fight Night Round 3—the closest thing to being in the ring without getting punched.",
      brand: "EASports",
      gamingSystem: "X-Box 360",
      genre: "Action",
      gameRating: "T",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dwce85980d/images/large/easports-fight-night-round-3-xbox360.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    FightNight.save(function (err) {
      if (err) return console.log(err);
    });

    const RobertLudlums = new NestedItemGames({
      title: "Robert Ludlum's: The Bourne Conspiracy(for X-Box 360)",
      price: 59.99,
      availability: "In Stock",
      description:
        "You are the perfect weapon. You are Jason Bourne. Robert Ludlum's best-selling spy novels and blockbuster film adaptations have thrilled millions. Now become Ludlum's most famous spy Jason Bourne, going deeper than ever into his world of espionage and conspiracy. A signature Jason Bourne video game experience is born in an original title fusing the viscous tension and depth of the novels with the aggressive style and frenetic action of the films.",
      brand: "Sierra",
      gamingSystem: "X-Box 360",
      genre: "Action",
      gameRating: "T",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dw4b91d9c8/images/large/sierra-the-bourne-conspiracy-xbox360.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    RobertLudlums.save(function (err) {
      if (err) return console.log(err);
    });

    const MarioKart = new NestedItemGames({
      title: "Mario Kart(for Nintendo DS)",
      price: 59.99,
      availability: "In Stock",
      description:
        "Mario, Luigi, Princess Peach, Wario, Yoshi, Donkey Kong, Toad and Bowser put the pedal to the go-kart metal in all-new tracks, plus many hidden courses. Grab coins to max out your speed and blast your rivals with the ever-popular arsenal of red, green and spiked Koopa shells.",
      brand: "Nintendo",
      gamingSystem: "Nintendo DS",
      genre: "Sports",
      gameRating: "E",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dwbdf65f48/images/large/nintendo-mario-kart-nintendods.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    MarioKart.save(function (err) {
      if (err) return console.log(err);
    });

    const MonopolyHereAndNow = new NestedItemGames({
      title: "Monopoly Here and Now: The World Edition(for Sony PS3)",
      price: 59.99,
      availability: "In Stock",
      description:
        "Celebrating over 70 years of family fun, the legendary property trading game MONOPOLY® is hitting video game consoles in a whole new way! The world’s most popular board game returns in 2008 with MONOPOLY® HERE & NOW: The World Edition, a modern makeover of the classic game. With a host of current-day cities and landmarks from all over the world, this new version will let players wheel and deal and compete to own some of the most recognized locales on Earth. EA brings this exciting new edition of the beloved board game to video game consoles for the first time along with entirely new features that focus on super-fast play, fun and interactive mini-games and four-player simultaneous gameplay. Players can now host the ultimate MONOPOLY night with family and friends.",
      brand: "EASports",
      gamingSystem: "Sony PS3",
      genre: "Strategy",
      gameRating: "E",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dw8af30e34/images/large/easports-monopoly-ps3.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    MonopolyHereAndNow.save(function (err) {
      if (err) return console.log(err);
    });

    const Warhawk = new NestedItemGames({
      title: "Warhawk(for Sony PS3)",
      price: 59.99,
      availability: "In Stock",
      description:
        "The long-awaited remake to the PlayStation hit takes flight on the PLAYSTATION3 (PS3) computer entertainment system. In Warhawk, players experience the thrill of white-knuckle aerial combat with swarms of enemy fighters, bone-crunching armored assaults and high-intensity infantry combat in a massive, all-out war fought both on the ground and in the skies.",
      brand: "Sony",
      gamingSystem: "Sony PS3",
      genre: "Action",
      gameRating: "T",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dwe5dca3ee/images/large/sony-warhawk-ps3.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    Warhawk.save(function (err) {
      if (err) return console.log(err);
    });

    const EternalSonata = new NestedItemGames({
      title: "Eternal Sonata(for Sony PS3)",
      price: 59.99,
      availability: "In Stock",
      description:
        "On his deathbed, the famous composer, Chopin, drifts between this life and the next. In his final hours, he experiences a fantastical dream where he encounters a young girl facing a terrible destiny and the boy who will fight to save her. On the border between dreams and reality, Chopin discovers the light that shines in all of us in this enduring tale of good and evil, love and betrayal.",
      brand: "Namco",
      gamingSystem: "Sony PS3",
      genre: " Role-Playing",
      gameRating: "T",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dwf941567e/images/large/namco-eternal-sonata-ps3.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    EternalSonata.save(function (err) {
      if (err) return console.log(err);
    });

    const NintendoWii = new NestedItemGameConsoles({
      title: "Nintendo Wii™ Game Console",
      price: 199.99,
      extendedWarranty: "1 Year Warranty",
      availability: "In Stock",
      description:
        "To truly understand how Wii revolutionizes gaming, you have to try it for yourself. Quite simply, Wii is for everyone. The ease of use and interactivity of the Wii Remote and Nunchuk allows for a unique social gaming experience for the whole family. You don't just play Wii, you experience it.",
      brand: "Nintendo",
      gamingSystem: "Wii",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dwcecf9061/images/large/nintendo-wii-console.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    NintendoWii.save(function (err) {
      if (err) return console.log(err);
    });

    const MicrosoftXBox360 = new NestedItemGameConsoles({
      title: "Microsoft X-Box 360 Game Console",
      price: 199.99,
      extendedWarranty: "1 Year Warranty",
      availability: "In Stock",
      description:
        "Xbox 360 puts you at the center of the most exciting games and entertainment experiences on earth. Xbox 360 not only has the best, highest-rated games, and the most robust title library, but it also offers access to the industry-leading Xbox LIVE service and tailored digital entertainment experiences that revolve around you.",
      brand: "Microsoft",
      gamingSystem: "X-Box 360",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dw937a580f/images/large/microsoft-xbox360-console.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    MicrosoftXBox360.save(function (err) {
      if (err) return console.log(err);
    });

    const SonyPlaystation3 = new NestedItemGameConsoles({
      title: "Sony Playstation 3 Game Console",
      price: 399.99,
      extendedWarranty: "1 Year Warranty",
      availability: "In Stock",
      description:
        "Includes PLAYSTATION 3 80GB system, DUALSHOCK 3 wireless controller, free PLAYSTATION Network membership, internet ready Wi-Fi, and 80GB of hard disk storage for all your games, music, videos, and photos. Every PS3 comes with a built-in Blu-ray player to give you the best high-definition viewing experience and pristine picture quality. Plus, the PS3 can play your entire catalog of CDs and DVDs.",
      brand: "Sony",
      gamingSystem: "Sony PS3",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dwa2cbd05d/images/large/sony-ps3-console.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    SonyPlaystation3.save(function (err) {
      if (err) return console.log(err);
    });

    const SonyPSP = new NestedItemGameConsoles({
      title: "Sony PSP Game Console",
      price: 149.99,
      extendedWarranty: "1 Year Warranty",
      availability: "In Stock",
      description:
        "The Sony PSP puts exciting games and cool multimedia features in the palm of your hand. Its large widescreen LCD display delivers vibrant, eye-catching 3-D graphics and full-motion video sequences, while its built-in speakers enhance your games with stereo sound. And the PSP's library features games spread over a number of different genres, so you're sure to find titles that suit your tastes.",
      brand: "Sony",
      gamingSystem: "Sony PSP",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dw3b835a04/images/large/sony-psp-console.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    SonyPSP.save(function (err) {
      if (err) return console.log(err);
    });

    const NintendoDS = new NestedItemGameConsoles({
      title: "Nintendo DS Game Console",
      price: 99.99,
      extendedWarranty: "1 Year Warranty",
      availability: "In Stock",
      description:
        "Nintendo DS revolutionizes handheld gameplay. With two ultra bright screens, 3D graphics, touch-screen technology and wireless communication, you can challenge yourself or your friends anywhere, anytime.",
      brand: "Nintendo",
      gamingSystem: "Nintendo DS",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dw3355cad4/images/large/nintendo-ds-console.jpg",
      defaultImage:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    NintendoDS.save(function (err) {
      if (err) return console.log(err);
    });

    const SonyPS3 = new NestedItemGameConsoles({
      title: "Sony Playstation 3 Game Console",
      price: 399.99,
      extendedWarranty: "1 Year Warranty",
      availability: "In Stock",
      description:
        "Includes PLAYSTATION 3 80GB system, DUALSHOCK 3 wireless controller, free PLAYSTATION Network membership, internet ready Wi-Fi, and 80GB of hard disk storage for all your games, music, videos, and photos. Every PS3 comes with a built-in Blu-ray player to give you the best high-definition viewing experience and pristine picture quality. Plus, the PS3 can play your entire catalog of CDs and DVDs.",
      brand: "Sony",
      gamingSystem: "Sony PS3",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/Sites-SiteGenesis-Site/-/default/dw62f0b85c/images/noimagelarge.png",
    });
    SonyPS3.save(function (err) {
      if (err) return console.log(err);
    });

    res.send("We created 2 nested categories for Gaming and Game Consoles");
  });
};

//-----------------------По-Гет-запросу----получить--все--данные-из--категории--Gaming---------------------------

exports.gaming_products = function (req, res) {
  NestedItemGameConsoles.findOne({ brand: "Nintendo" })
    .populate("parentCategory") //подменяет идентификатор  информацией!
    .exec(function (err, parent) {
      if (err) return console.log(err);
      console.log("Parent category is %s", parent.parentCategory.name);
      res.send(parent);
    });
};

exports.electronicsNested_children = function (req, res) {
  Gaming.find({})
    .populate("nestedItemsGames") //подменяет идентификатор  информацией!
    .exec(function (err, doc) {
      if (err) return console.log(err);
      console.log("nested are", doc.nestedItemsGames);
      console.log(doc);
    });

  res.send("Yehh");
};

//-----------------------По-Гет-запросу----получить--все-данные-из--категории--NestedItemGames--------------
exports.gaming_Game_consoles = function (req, res) {
  //при клике на navbarItem
  NestedItemGameConsoles.find({}, function (err, doc) {
    if (err) return console.log(err);
    console.log("We found products from Gaming->Games", doc);
    res.send(doc);
  });
};

//-----------------------По-Гет-запросу----сортировать по-убыванию--NestedItemGames--------------
exports.gaming_Games_sort = function (req, res) {
  NestedItemGames.find()
    .sort({ price: -1 })
    //.select("price title")
    .exec(function (err, doc) {
      if (err) return console.log(err);
      console.log("Rezult of sorting by desc", doc);
      res.send(doc);
    });
};
//-----------------------По-Гет-запросу----сортировать по-убыванию--NestedItemGameConsoles--------------
exports.gaming_Game_consoles_sort = function (req, res) {
  NestedItemGameConsoles.find()
    .sort({ price: -1 })
    //.select("price title")
    .exec(function (err, doc) {
      if (err) return console.log(err);
      console.log("We found products from Gaming->Game Consoles", doc);
      res.send(doc);
    });
};

//-----------------------По-Гет-запросу----пагинация-в-категории--NestedItemGameConsoles--------------
(exports.allproducts = paginatedData(NestedItemGames)),
  (req, res) => {
    res.json(res.paginatedData);
  };

function paginatedData(model) {
  return async (req, res, next) => {
    const page = parseInt(req.query.page);
    const limit = parseInt(req.query.limit);
    const sortBy = req.query.sortBy;
    const color = req.query.color;
    const size = req.query.size;
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;
    const results = {};

    if (endIndex < (await model.countDocuments().exec())) {
      results.next = {
        page: page + 1,
        limit: limit,
      };
    }

    if (startIndex > 0) {
      results.previous = {
        page: page - 1,
        limit: limit,
      };
    }
    results.total = await model.countDocuments().exec();
    results.sortBy = sortBy;
    results.color = color;
    results.size = size;

    if (sortBy === "Price Hight To Low") {
      try {
        results.result = await model
          .find()
          .limit(limit)
          .skip(startIndex)
          .sort({ price: -1 })
          .exec();
        res.paginatedData = results;
        console.log("Endpoint Allproducts, paginated", sortBy, color, size);
        res.json(results);
        next();
      } catch (e) {
        res.status(500).json({ message: e.message });
      }
    }
    if (sortBy === "Brand") {
      try {
        results.result = await model
          .find()
          .limit(limit)
          .skip(startIndex)
          .sort({ brand: 1 })
          .exec();
        res.paginatedData = results;
        console.log("Endpoint Allproducts, paginated", sortBy, color, size);
        res.json(results);
        next();
      } catch (e) {
        res.status(500).json({ message: e.message });
      }
    }
    if (sortBy === "Price Low To Hight") {
      try {
        results.result = await model
          .find()
          .limit(limit)
          .skip(startIndex)
          .sort({ price: 1 })
          .exec();
        res.paginatedData = results;
        console.log("Endpoint Allproducts, paginated", sortBy, color, size);
        res.json(results);
        next();
      } catch (e) {
        res.status(500).json({ message: e.message });
      }
    }
    if (sortBy === "Product Name Z-A") {
      try {
        results.result = await model
          .find()
          .limit(limit)
          .skip(startIndex)
          .sort({ title: -1 })
          .exec();
        res.paginatedData = results;
        console.log("Endpoint Allproducts, paginated", sortBy, color, size);
        res.json(results);
        next();
      } catch (e) {
        res.status(500).json({ message: e.message });
      }
    }

    if (sortBy === "Product Name A-Z") {
      try {
        results.result = await model
          .find()
          .limit(limit)
          .skip(startIndex)
          .sort({ title: 1 })
          .exec();
        res.paginatedData = results;
        console.log("Endpoint Allproducts, paginated", sortBy, color, size);
        res.json(results);
        next();
      } catch (e) {
        res.status(500).json({ message: e.message });
      }
    } else {
      try {
        results.result = await model
          .find()
          .limit(limit)
          .skip(startIndex)
          .exec();
        res.paginatedData = results;
        console.log("Endpoint Allproducts, paginated", sortBy, color, size);
        res.json(results);

        next();
      } catch (e) {
        res.status(500).json({ message: e.message });
      }
    }
  };
}
//-----------------------По-Гет-запросу----отображение-данных-из-коллекции-NestedItemGameConsoles-по _id-----------
exports.gaming_Game_consoles_search_By_id = function (req, res) {
  const id = req.params.id;

  NestedItemGameConsoles.findOne({ _id: id }, function (err, doc) {
    if (err) return console.log(err);
    console.log(`We found this obj ${doc}`);
    res.send(doc);
  });
};

//-----------------------По-Гет-запросу----отображение-данных-из-коллекции-NestedItemGames----------по _id-----------
exports.searchBy_id = function (req, res) {
  const id = req.params.id;
  NestedItemGames.findOne({ _id: id }, function (err, doc) {
    if (err) return console.log(err);
    console.log(`We found this obj ${doc}`);
    res.send(doc);
  });
};

//-----------------------По-Гет-запросу----отображение данных из коллекции NestedItemGames по значению поля "title" ----------
exports.searchProducts_title = function (req, res) {
  const productName = req.params.title;
  NestedItemGames.findOne(
    { title: productName },
    "title availability description image price brand gamingSystem gameRating genre",
    //_id поиск по ид БД
    function (err, doc) {
      if (err) return console.log(err, "Something bad happend");
      res.send(doc);
    }
  );
};

//-----------------------По-Гет-запросу--поиск с 2-мя параметрами----------------------------------------------
exports.categories_categoryId_products_productId = function (req, res) {
  let categoryId = req.params.categoryId;
  let productId = req.params.productId;
  console.log("Without implementation in DB");
  res.send(`Результат поиска в Категории: ${categoryId}  Товар: ${productId}`);
};

//-----------------------По-Гет-запросу----отображение-данных-для-сортировки-electronics--------
exports.sorting = function (req, res) {
  let filterKey = req.query.filterKey;
  ElectronicFilters.find()
    //.find({ name: "subcategory" })
    //.select("sortingvalues name")
    .exec(function (err, doc) {
      if (err) return console.log(err);
      console.log("Rezult of sorting", doc, filterKey);
      res.send(doc);
    });
};
