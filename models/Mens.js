const { Schema, model, Types } = require("mongoose");

const menSchema = new Schema({
  name: { type: String, required: true },
  sortingvalues: { type: Array, required: true },
  versionKey: false, //все равно создалась версия _v
});

const Mens = model("Mens", menSchema);

module.exports.Mens = Mens;
