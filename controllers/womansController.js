const { Womans, Femaleproducts } = require("../models/Womans");

exports.sorting = function (req, res) {
  let filterKey = req.query.filterKey;
  Womans.find()
    //.find({ name: "subcategory" })
    //.select("sortingvalues name")
    .exec(function (err, doc) {
      if (err) return console.log(err);
      console.log("Rezult of sorting", doc, filterKey);
      res.send(doc);
    });
};
//-----------------------По-Гет-запросу----отображение-данных-из-Femaleproducts-------
exports.femaleproducts = function (req, res) {
  Femaleproducts.find().exec(function (err, doc) {
    if (err) return console.log(err);
    console.log(`List of products for woman`);
    res.send(doc);
  });
};

//-----------------------По-Гет-запросу----отображение-данных-из-Femaleproducts-------для подальших змін
exports.femaleproducts_filter = async function (req, res) {
  let filterKey = req.params.filterKey;
  Femaleproducts.find()
    .where("type")
    .equals(filterKey)
    .where("details.sole")
    .equals("Гума")
    .where("details.price")
    .gt(1000)
    .lt(2000)

    //{details: { $elemMatch: { price: 2149.57, sole: "Гума" } },}
    //.find({ name: "subcategory" })
    //.select("sortingvalues name")
    .exec(function (err, doc) {
      if (err) return console.log(err);
      console.log(`List of products for woman ${doc}, params ${filterKey}`);
      res.send(doc);
    });
};

//-----------------------По-Гет-запросу----пагинация-в-категории--NestedItemGameConsoles--------------
(exports.allproducts = paginatedData(Femaleproducts)),
  (req, res) => {
    res.json(res.paginatedData);
  };

function paginatedData(model) {
  return async (req, res, next) => {
    const page = parseInt(req.query.page);
    const limit = parseInt(req.query.limit);
    const sortBy = parseInt(req.query.sortBy);
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;
    const results = {};

    if (endIndex < (await model.countDocuments().exec())) {
      results.next = {
        page: page + 1,
        limit: limit,
        sortBy: sortBy,
      };
    }

    if (startIndex > 0) {
      results.previous = {
        page: page - 1,
        limit: limit,
        sortBy: sortBy,
      };
    }
    results.total = await model.countDocuments().exec();

    try {
      results.result = await model.find().limit(limit).skip(startIndex).exec();
      res.paginatedData = results;
      res.json(results);

      next();
    } catch (e) {
      res.status(500).json({ message: e.message });
    }
  };
}

//-----------------------По-Гет-запросу----отображение-данных-из-коллекции-Femaleproducts-----по _id-----------
exports.searchBy_id = function (req, res) {
  const id = req.params.id;
  Femaleproducts.findOne({ _id: id }, function (err, doc) {
    if (err) return console.log(err);
    console.log(`We found this obj ${id}`);
    res.send(doc);
  });
};

//-----------------------По-Гет-запросу----отображение данных из коллекции NestedItemGames по значению поля "title" ----------
exports.searchProducts_title = function (req, res) {
  const productName = req.params.title;
  Femaleproducts.findOne({ name: productName }, function (err, doc) {
    if (err) return console.log(err);
    console.log(`We found this obj ${doc}`);
    res.send(doc);
  });
};
