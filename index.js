const express = require("express");
const config = require("config");
const mongoose = require("mongoose");
const cors = require("cors");
const cookieParser=require("cookie-parser");
const app = express();
const errorMiddleware=require("./middleware/error-middleware");



app.use(express.json({ extended: true }));
app.use(cookieParser());
app.use(cors());



app.use(
  "/api/products/category/electronics",
  require("./routes/electronics.routes")
);
app.use(
  "/api/products/category/newarrivals",
  require("./routes/new_arrivals.routes")
);

app.use("/api/products/category/mens", require("./routes/mens.routes"));
app.use("/api/products/category/womans", require("./routes/womans.routes"));
app.use("/api", require("./routes/auth.routes"));
/*app.use(
  "/api/products/category/top_sellers",
  require("./routes/top_sellers.routes")
);*/
app.use(errorMiddleware);

app.get("/api", function (req, res) {
  res.send("API is running");
});

const PORT = config.get("port") || 5000;
async function start() {
  try {
    await mongoose.connect(config.get("mongoUri"), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
    app.listen(PORT, () =>
      console.log(`Application is started on port ${PORT}`)
    );
  } catch (e) {
    console.log("Server error", e.message);
    process.exit(1);
  }
}
start();
