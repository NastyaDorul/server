const { Schema, model, Types } = require("mongoose");

const womanSchema = new Schema({
  name: { type: String, required: true },
  sortingvalues: { type: Array, required: true },
  versionKey: false, //все равно создалась версия _v
});

const femaleproducts = new Schema({
  type: { type: String, required: true },
  versionKey: false,
});

const Womans = model("Womans", womanSchema);
const Femaleproducts = model("Femaleproducts", femaleproducts);

module.exports.Womans = Womans;
module.exports.Femaleproducts = Femaleproducts;
