//const userModel=require("../models/user-model");
const {userModel}=require("../models/user-model");
const bcrypt=require("bcryptjs");
const uuid=require("uuid");
const mailService=require("../service/mail-service");
const tokenService=require("../service/token-service");
const UserDto=require("../dtos/user-dto");
const config=require("config");
const ApiError=require("../exeptions/api-errors");



class UserService{
    async registration(email,password){
        const candidate= await userModel.findOne({email:email})
        if(candidate){
            throw ApiError.BedRequest(`User whith email ${email} is already exist`)
        }
        const hashedPassword = await bcrypt.hash(password, 12);
        const activationLink=uuid.v4();//random string
        const user=await userModel.create({email,password: hashedPassword,activationLink})
        await mailService.sendActivationMail(email,`${config.get("api_url")}/api/activate/${activationLink}`);

        const userDto=new UserDto(user);//id,email,isActivated
        const tokens=tokenService.generateTokens({...userDto});//return obj tokens={accessToken,refreshToken}
        console.log(tokens);
        await tokenService.saveTokens(userDto.id,tokens.refreshToken);
        return{...tokens, user: userDto}
    }
    async activate(activationLink){
        const user = await userModel.findOne({activationLink})
        if(!user){
            throw ApiError.BedRequest("Uncorrect activation Link");
        }
        user.isActivated=true;
        await user.save();
    }

    async login(email,password){
        const user=await userModel.findOne({email})
        if(!user){
            throw ApiError.BedRequest(`User with email ${email} not found`);
        }
        const isPassEquals=await bcrypt.compare(password,user.password);
        if(!isPassEquals){
            throw ApiError.BedRequest("Uncorrect user password");
        }
        const userDto=new UserDto(user);
        const tokens=tokenService.generateTokens({...userDto});
        await tokenService.saveTokens(userDto.id,tokens.refreshToken);
        return{...tokens, user: userDto}

    }
    async logout(refreshToken){
        const token=await tokenService.removeToken(refreshToken);
        return token;

    }
    async refresh(refreshToken){
        if(!refreshToken){
            throw ApiError.UnauthorizedError();
        }
        const userData=tokenService.validateRefreshToken(refreshToken);
        const tokenFromDB=await tokenService.findToken(refreshToken);
        if(!userData || !tokenFromDB){
            throw ApiError.UnauthorizedError();
        }
        const user=await userModel.findById(userData.id)
        const userDto=new UserDto(user);
        const tokens=tokenService.generateTokens({...userDto});
        await tokenService.saveTokens(userDto.id,tokens.refreshToken);
        return{...tokens, user: userDto}

    }

    async getAllUsers(){
        const users = await userModel.find();
        return users;
    }

}

module.exports=new UserService();