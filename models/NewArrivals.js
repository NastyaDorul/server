const { Schema, model, Types } = require("mongoose");

const arrivalsSchema = new Schema({
  name: { type: String, required: true },
  sortingvalues: { type: Array, required: true },
  versionKey: false, //все равно создалась версия _v
});

const Newarrivals = model("Newarrivals", arrivalsSchema);

module.exports.Newarrivals = Newarrivals;
