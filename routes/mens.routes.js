const { Router } = require("express");

const { Mens } = require("../models/Mens");
const router = Router();

//-----------------------По-Гет-запросу----отображение-данных-для-сортировки-mens--------
router.get("/sorting", function (req, res) {
  let filterKey = req.query.filterKey;
  Mens.find()
    //.find({ name: "subcategory" })
    //.select("sortingvalues name")
    .exec(function (err, doc) {
      if (err) return console.log(err);
      console.log("Rezult of sorting", doc, filterKey);
      res.send(doc);
    });
});

module.exports = router;
