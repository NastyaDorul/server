const { Router } = require("express");

const router = Router();

// Require controller modules.
var electronics_controller = require("../controllers/electronicsController");

//-По-Гет-запросу----добавляю данные в БД
router.get("/gaming-create", electronics_controller.gaming_create);

//-По-Гет-запросу----получить--все--данные-из--категории--Gaming
router.get("/gaming-products", electronics_controller.gaming_products);

router.get(
  "/electronics/nested-children",
  electronics_controller.electronicsNested_children
);

//-По-Гет-запросу----получить--все-данные-из--категории--NestedItemGames
router.get(
  "/gaming-Game-consoles",
  electronics_controller.gaming_Game_consoles
);

//-По-Гет-запросу----сортировать по-убыванию--NestedItemGames
router.get("/gaming-Games-sort", electronics_controller.gaming_Games_sort);

//-По-Гет-запросу----сортировать по-убыванию--NestedItemGameConsoles
router.get(
  "/gaming-Game-consoles-sort",
  electronics_controller.gaming_Game_consoles_sort
);

//-По-Гет-запросу--пагинация-в-категории--NestedItemGameConsoles
router.get("/allproducts", electronics_controller.allproducts);

//-По-Гет-запросу-отображение-данных-из-коллекции-NestedItemGameConsoles-по _id
router.get(
  "/gaming-Game-consoles/search-By/:id",
  electronics_controller.gaming_Game_consoles_search_By_id
);

//-По-Гет-запросу-отображение-данных-из-коллекции-NestedItemGames-по _id
router.get("/search-By/:id", electronics_controller.searchBy_id);

//-По-Гет-запросу----отображение данных из коллекции NestedItemGames по значению поля "title"
router.get(
  "/search/products/:title",
  electronics_controller.searchProducts_title
);

//-По-Гет-запросу--поиск с 2-мя параметрами-

router.get(
  "/categories/:categoryId/products/:productId",
  electronics_controller.categories_categoryId_products_productId
);

//-По-Гет-запросу----отображение-данных-для-сортировки-electronics
router.get("/sorting", electronics_controller.sorting);

module.exports = router;
