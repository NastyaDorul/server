const { Schema, model, Types } = require("mongoose");

const gamingSchema = new Schema({
  name: { type: String, required: true },
  nestedItemsGames: [{ type: Schema.Types.ObjectId, ref: "NestedItemGames" }],
  nestedItemsGameConsoles: [
    { type: Schema.Types.ObjectId, ref: "NestedItemGameConsoles" },
  ],
  versionKey: false, //все равно создалась версия _v
});

const nestedItemsGamesSchema = new Schema(
  {
    title: { type: String, required: true },
    price: { type: Number, required: true },
    availability: { type: String, required: true },
    description: { type: String },
    brand: { type: String },
    gamingSystem: { type: String },
    genre: { type: String },
    gameRating: { type: String },
    parentCategory: [{ type: Schema.Types.ObjectId, ref: "Gaming" }],
    image: { type: Object },
    defaultImage: { type: Object },
    versionKey: false, //все равно создалась версия _v
  },
  { timestamps: true }
);
nestedItemsGamesSchema.statics.findByTitle = function (name, cb) {
  return this.find({ name: new RegExp(name, "i") }, cb);
};

const nestedItemsGameConsolesSchema = new Schema(
  {
    title: { type: String, required: true },
    price: { type: Number, required: true },
    extendedWarranty: { type: Schema.Types.Mixed },
    availability: { type: String, required: true },
    description: { type: String },
    brand: { type: String },
    gamingSystem: { type: String },
    parentCategory: [{ type: Schema.Types.ObjectId, ref: "Gaming" }],
    image: { type: Object }, //?
    defaultImage: { type: Object },

    versionKey: false, //все равно создалась версия _v
  },
  { timestamps: true }
);

const electronicsSchema = new Schema({
  name: { type: String, required: true },
  sortingvalues: { type: Array, required: true },
  versionKey: false, //все равно создалась версия _v
});

const Gaming = model("Gaming", gamingSchema);
const NestedItemGames = model("NestedItemGames", nestedItemsGamesSchema);
const NestedItemGameConsoles = model(
  "NestedItemGameConsoles",
  nestedItemsGameConsolesSchema
);
const ElectronicFilters = model("ElectronicFilters", electronicsSchema);

module.exports.Gaming = Gaming;
module.exports.NestedItemGames = NestedItemGames;
module.exports.NestedItemGameConsoles = NestedItemGameConsoles;
module.exports.ElectronicFilters = ElectronicFilters;
