const jwt=require("jsonwebtoken");
const config = require("config");
//const tokenModel=require("../models/token-model");
const {tokenModel}=require("../models/token-model");

class TokenService{
    generateTokens(payload){
        const accessToken=jwt.sign(payload,config.get("jwtAccessSecret"),{ expiresIn: "1h" })
        const refreshToken=jwt.sign(payload,config.get("jwtRefreshSecret"),{expiresIn: "30d"})
        return {
            accessToken,
            refreshToken
        }
    }
    validateAccessToken(token){
        try{
            const userData=jwt.verify(token, config.get("jwtAccessSecret"));
            return userData;
        }catch(e){
            return null
        }
    }

    validateRefreshToken(token){
        try{
            const userData=jwt.verify(token,config.get("jwtRefreshSecret"));
            return userData;
        }catch(e){
            return null;
        }
    }
    async saveTokens(userId,refreshToken){
        const tokenData = await tokenModel.findOne({user:userId})
        if(tokenData){
            tokenData.refreshToken=refreshToken;
            console.log(tokenData.refreshToken);
            return tokenData.save();
        }
        const token=await tokenModel.create({user:userId,refreshToken})
        return token;
    }
    async removeToken(refreshToken){
        const tokenData=await tokenModel.deleteOne({refreshToken});
        return tokenData;
    }

    async findToken(refreshToken){
        const tokenData=await tokenModel.findOne({refreshToken});
        return tokenData;
    }


}

module.exports=new TokenService();